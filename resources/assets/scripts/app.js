/**
 * External Dependencies
 */
import 'jquery';
import 'bootstrap';

$(document).ready(() => {
    // console.log('Hello world');

    const header = document.querySelector(".header");
    const headerScrollUp = "header--up";
    const headerScrollDown = "header--down";
    let lastScroll = 0;

    window.addEventListener("scroll", () => {
        const currentScroll = window.pageYOffset;
        if (currentScroll <= 0) {
            header.classList.remove(headerScrollUp);
            return;
        }

        if (currentScroll > lastScroll && !header.classList.contains(headerScrollDown)) {
            // down
            header.classList.remove(headerScrollUp);
            header.classList.add(headerScrollDown);
        } else if (currentScroll < lastScroll && header.classList.contains(headerScrollDown)) {
            // up
            header.classList.remove(headerScrollDown);
            header.classList.add(headerScrollUp);
        }
        lastScroll = currentScroll;
    });
});