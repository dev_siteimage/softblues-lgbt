<footer class="footer">
  <div class="container">
    <div class="footer__wrapper">
      @php(dynamic_sidebar('sidebar-footer'))
    </div>
  </div>
</footer>
