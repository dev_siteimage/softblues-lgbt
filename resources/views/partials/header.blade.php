<header class="banner header">
  <div class="container-fluid">
    <div class="header__wrapper">
      <div class="header__logo">
        <a class="brand header__logo-link" href="{{ home_url('/') }}">CREATE <br/> YOUR GUIDE
        </a>
      </div>
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => ' header-menu', 'echo' => false]) !!}
        @endif
      </nav>
      <div class="header__button">
        <button type="button" class="button button--accent button--small">Create your guide</button>
      </div>
    </div>

  </div>
</header>
