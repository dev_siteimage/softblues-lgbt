@include('partials.header')

<div class="page-body">
  <div class="container">
    <main class="main">
      @yield('content')
    </main>
  
    @hasSection('sidebar')
      <aside class="sidebar">
        @yield('sidebar')
      </aside>
    @endif
  </div>
</div>

@include('partials.footer')
